import {createHttpRequestReducer} from "../util/createHttpRequestReducer";
import {createHttpRequestActionBundle} from "../util/createHttpActionBundle";

export default class PodkatActions {
    static PODKAT_REQUESTED = 'PODKAT_REQUESTED';
    static PODKAT_RECEIVED = 'PODKAT_RECEIVED';
    static PODKAT_ERROR = 'PODKAT_ERROR';

    static getPodkat() {
        return createHttpRequestActionBundle({
                method: 'get',
                url: 'podkats',
                timeout: 40000,
                responseType: 'json',
            },
            PodkatActions.PODKAT_REQUESTED,
            PodkatActions.PODKAT_RECEIVED,
            PodkatActions.PODKAT_ERROR
        )();
    }

    static PREDICT_REQUESTED = 'PREDICT_REQUESTED';
    static PREDICT_RECEIVED = 'PREDICT_RECEIVED';
    static PREDICT_ERROR = 'PREDICT_ERROR';

    static getPredict(bodyParams) {
        return createHttpRequestActionBundle({
                method: 'post',
                url: 'calculate',
                timeout: 40000,
                responseType: 'json',
                data:bodyParams
            },
            PodkatActions.PREDICT_REQUESTED,
            PodkatActions.PREDICT_RECEIVED,
            PodkatActions.PREDICT_ERROR
        )();
    }
}
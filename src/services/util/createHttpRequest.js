import moment from "moment";
import axios from "axios";

export function createHttpRequest(settings) {
    const request = axios({
        method: settings.method,
        url: settings.url,
        timeout: 40000,
        responseType: 'json',
        data: settings.data,
    });
    return request;
}



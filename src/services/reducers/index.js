import {combineReducers} from 'redux';
import podkatReducers from "./PodkatReducer";

const rootReducer = combineReducers({
    podkatReducers
});

export default rootReducer;
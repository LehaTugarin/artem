import {combineReducers} from 'redux';
import {PodkatActions} from "../actions";
import {createHttpRequestReducer} from "../util/createHttpRequestReducer";

const podkatData = createHttpRequestReducer(
    PodkatActions.PODKAT_REQUESTED,
    PodkatActions.PODKAT_RECEIVED,
    PodkatActions.PODKAT_ERROR,
    []
);

const predictData = createHttpRequestReducer(
    PodkatActions.PREDICT_REQUESTED,
    PodkatActions.PREDICT_RECEIVED,
    PodkatActions.PREDICT_ERROR,
    []
);

const heatListReducers = combineReducers({
    podkatData,
    predictData
});

export default heatListReducers;
import React, { PureComponent } from 'react';
import { Link, Redirect, Route, Switch, withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import { Navbar, Nav, NavbarBrand, NavDropdown, NavItem, Form, FormControl, Button, Container } from 'react-bootstrap';
import { Main } from "./screens";
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import './App.scss';
class App extends PureComponent {
  render() {
    const { pathname } = this.props.location;
    return (
      <div className="App">
        <Navbar bg="primary" variant="dark" expand="lg">
          <Navbar.Brand href="/desk">Site Name</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">


            <Nav className="mr-auto">
              <Nav.Link active={this.props.location.pathname == '/desk' ? true : false} href="/desk">hfdp</Nav.Link>

              <Nav.Link active={this.props.location.pathname == '/analytics' ? true : false} href="/analytics">skjdf</Nav.Link>
            </Nav>


          </Navbar.Collapse>
        </Navbar>
        <Switch localtion={this.props.location}>
          <Route exact={true} path={"/"} render={props => (<Redirect
            to={{
              pathname: "/desk",
              state: { from: props.location }
            }}
          />)} />
          <Route exact={true} path={"/login"} component={Main} />
          <Route exact={true} path={"/desk"} component={Main} />

        </Switch>
      </div>
    )
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));

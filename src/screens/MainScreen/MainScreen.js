import React, { PureComponent } from 'react';
import { Link, Redirect, Route, Switch, withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import { PodkatActions } from "../../services/actions";
import 'react-virtualized/styles.css';
import { Column, Table } from 'react-virtualized';

import { Navbar, Nav, NavbarBrand, NavDropdown, Row, Form, FormControl, Button, Container, Col } from 'react-bootstrap';

class MainScreen extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            index: -1,
        }
    }

    handleRowSelect(event) {
        this.setState(
            {
                index: event.index
            }
        )
    }

    rowStyleFormat(row) {
        if (row.index < 0) return;
        if (this.state.index === row.index) {
            return {
                backgroundColor: '#b7b9bd',
                color: '#333'
            };
        }
        return {
            backgroundColor: '#fff',
            color: '#333'
        };
    }

    componentDidMount() {
        this.props.loadPodkat();
    }

    calculate() {
        let params = [{
            id: 706,
            name: 'Коэф. уд. магн. протерь Р1.5/50',
            value: this.koefMagnLoss.value
        }, {
            id: 520,
            name: 'Маг. проницаемость m 1.5/50',
            value: this.magnPron.value
        }, {
            id: 784,
            name: 'Коэрцитивная сила, Hc1.5/50',
            value: this.coerc.value
        }, {
            id: 620,
            name: 'Магнитная индукция',
            value: this.induction.value
        },
        ];

        let data={
            podkat: this.props.podkatData.data[this.state.index],
            params: params
        }
        this.props.calculate(data);
    }

    render() {
        const { pathname } = this.props.location;

        let data = [];
        if (this.props.podkatData.data) {
            this.props.podkatData.data.forEach(element => {
                data.push({
                    partnum: element.partnum,
                    C: element.chemistry[0].value,
                    Ni: element.chemistry[1].value,
                    N: element.chemistry[2].value,
                    S: element.chemistry[3].value,
                    Mn: element.chemistry[4].value,
                    F: element.chemistry[5].value,
                    Al: element.chemistry[6].value,
                    Cu: element.chemistry[7].value,
                    Si: element.chemistry[8].value,
                    Xr: element.chemistry[9].value,
                    Ti: element.chemistry[10].value
                })
            });
        }

        return (
            <div>
                <Container>
                    <Row>
                        <h2 style={{ flex: 1 }}>Подкат</h2>
                        <Form inline>
                            <FormControl type="text" placeholder="Search" className=" mr-sm-2" />
                            <Button type="submit">Поиск</Button>
                            <Button variant="light" type="submit">Справка</Button>
                        </Form>
                    </Row>

                    {this.props.podkatData.data ?
                        <div style={{ marginTop: 20 }}>
                            <Table
                                width={1200}
                                height={300}
                                headerHeight={20}
                                rowHeight={30}
                                rowCount={data.length}
                                rowGetter={({ index }) => data[index]}
                                onRowClick={this.handleRowSelect.bind(this)}
                                rowStyle={this.rowStyleFormat.bind(this)}>
                                <Column label="Партия" dataKey="partnum" width={200} />
                                <Column label="Углерод" dataKey="C" width={200} />
                                <Column label="Никель" dataKey="Ni" width={200} />
                                <Column label="Азот" dataKey="N" width={200} />
                                <Column label="Сера" dataKey="S" width={200} />
                                <Column label="Марганец" dataKey="Mn" width={200} />
                                <Column label="Фосфор" dataKey="F" width={200} />
                                <Column label="Алюминий" dataKey="Al" width={200} />
                                <Column label="Кремний" dataKey="Si" width={200} />
                                <Column label="Хром" dataKey="Xr" width={200} />
                                <Column label="Титан" dataKey="Ti" width={200} />
                            </Table>,

                        </div> : 'Load***'}
                    <Row>
                        <h2 style={{ flex: 1 }}>Партия динамной стали</h2>
                        <div style={{ display: 'flex' }}>
                            <label>
                                Коэф. уд. магн. протерь Р1.5/50:
                                <input type="text" ref={(input) => this.koefMagnLoss = input} />
                            </label>
                            <label>
                                Маг. проницаемость m1.5/50:
                                <input type="text" ref={(input) => this.magnPron = input} />
                            </label>
                            <label>
                                Коэрцитивная сила, Hc1.5/50:
                                <input type="text" ref={(input) => this.coerc = input} />
                            </label>
                            <label>
                                Магнитная индукция:
                                <input type="text" ref={(input) => this.induction = input} />
                            </label>
                        </div>


                        <Button type="submit" onClick={() => this.calculate()}>Расчет параметров</Button>
                    </Row>


                    <Row>
                        <h2 style={{ flex: 1 }}>Обработка на агрегатах</h2>

                        <Row>
                            <Col>Binary found at /Users/user/work/artem/artem/node_modules/node-sass/vendor/darwin-x64-83/binding.node
                            Testing binary
                            Binary is fine
                            npm WARN bootstrap@4.5.0 requires a peer of jquery@1.9.1 - 3 but none is installed. You must install peer dependencies yourself.
                            npm WARN bootstrap@4.5.0 requires a peer of popper.js@^1.16.0 but none is installed. You must install peer dependencies yourself.
npm WARN tsutils@3.17.1 requires a peer of typescript@>=2.8.0 || >= 3.2.0-dev || >= 3.3.0-dev || >= 3.4.0-dev || >= 3.5.0-dev || >= 3.6.0-dev || >= 3.6.0-beta || >= 3.7.0-dev || >= 3.7.0-beta but none is installed. You must install peer dependencies yourself.</Col>
                            <Col>Binary found at /Users/user/work/artem/artem/node_modules/node-sass/vendor/darwin-x64-83/binding.node
                            Testing binary
                            Binary is fine
                            npm WARN bootstrap@4.5.0 requires a peer of jquery@1.9.1 - 3 but none is installed. You must install peer dependencies yourself.
                            npm WARN bootstrap@4.5.0 requires a peer of popper.js@^1.16.0 but none is installed. You must install peer dependencies yourself.
npm WARN tsutils@3.17.1 requires a peer of typescript@>=2.8.0 || >= 3.2.0-dev || >= 3.3.0-dev || >= 3.4.0-dev || >= 3.5.0-dev || >= 3.6.0-dev || >= 3.6.0-beta || >= 3.7.0-dev || >= 3.7.0-beta but none is installed. You must install peer dependencies yourself.</Col>
                        </Row>

                    </Row>

                </Container>
            </div >
        )
    }
}

const mapStateToProps = state => ({
    podkatData: state.podkatReducers.podkatData
});

const mapDispatchToProps = dispatch => ({
    loadPodkat: () => {
        dispatch(PodkatActions.getPodkat())
    },
    calculate:(data)=>{
        dispatch(PodkatActions.getPredict(data))
    }
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainScreen));
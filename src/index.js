import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import {applyMiddleware, compose, createStore} from 'redux';
import {Provider} from 'react-redux';
import {Router} from 'react-router-dom';
import {createBrowserHistory as createHistory} from 'history';

import './index.css';
import App from './App';

import rootReducer from './services/reducers';

import * as serviceWorker from './serviceWorker';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middleware = process.env.NODE_ENV === 'production' ? [thunk] : [thunk];
export const store = createStore(rootReducer, composeEnhancers(applyMiddleware(...middleware)));
export const history = createHistory();


ReactDOM.render(<Provider store={store}>
        <Router history={history}>
            <App/>
        </Router>
    </Provider>,
    document.getElementById('root')
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
